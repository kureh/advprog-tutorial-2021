package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import org.springframework.boot.autoconfigure.web.ResourceProperties;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
    }

    @Override
    public void cast() {
        for(Spell s : spells){
            s.cast();
        }
    }

    @Override
    public void undo() {
        while(!spells.isEmpty()){
            this.spells.get(spells.size()-1).cast();
            this.spells.remove(spells.size()-1);
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
