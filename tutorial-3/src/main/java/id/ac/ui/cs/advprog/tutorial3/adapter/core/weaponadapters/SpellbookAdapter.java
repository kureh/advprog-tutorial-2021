package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean largeSpell;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        largeSpell = false;
    }

    @Override
    public String normalAttack() {
        largeSpell = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(!largeSpell){
            largeSpell = true;
            return spellbook.largeSpell();
        }else{
            return "Can't do that";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
