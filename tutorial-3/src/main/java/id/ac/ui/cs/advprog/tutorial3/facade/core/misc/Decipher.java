package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;

public class Decipher {
    private AbyssalTransformation at;
    private CaesarTransformation ctt;
    private  CelestialTransformation ct;

    public Decipher(){
        at = new AbyssalTransformation();
        ctt = new CaesarTransformation();
        ct = new CelestialTransformation();
    }

    public String encode(String text){
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        spell = at.encode(spell);
        spell = ct.encode(spell);
        spell = ctt.encode(spell);
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());

        return spell.getText();
    }

    public String decode(String code){
        Spell spell = new Spell(code, RunicCodex.getInstance());
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());
        spell = ctt.decode(spell);
        spell = ct.decode(spell);
        spell = at.decode(spell);

        return spell.getText();
    }

}
