package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    private boolean init = true;

    // TODO: implement me


    @Override
    public List<Weapon> findAll() {
        if(init){
            for(Spellbook spell : spellbookRepository.findAll()){
                weaponRepository.save(new SpellbookAdapter(spell));
            }
            for(Bow bow : bowRepository.findAll()){
                weaponRepository.save(new BowAdapter(bow));
            }
            init = false;
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    // attackType = 1 == chargedAttack
    // attackType = 2 == normalAttack
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if(attackType == 1){
            logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + " (charged attack): " + weapon.chargedAttack());
        }else if(attackType == 2){
            logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + " (normal attack): " + weapon.normalAttack());
        }
        weaponRepository.save(weapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
