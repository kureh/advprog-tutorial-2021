package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import java.lang.*;

public class CaesarTransformation {
    private int key;


    public CaesarTransformation(){
        this.key = 13;
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? 1 : -1;
        StringBuffer result = new StringBuffer();

        for (int i=0; i<text.length(); i++)
        {
            if (Character.isUpperCase(text.charAt(i)))
            {
                char ch = (char)(((int)text.charAt(i) + Math.abs((key*selector)) - 65) % 26 + 65);
                result.append(ch);
            }
            else if(Character.isLowerCase(text.charAt(i)))
            {
                char ch = (char)(((int)text.charAt(i) + Math.abs((key*selector)) - 97) % 26 + 97);
                result.append(ch);
            }
            else{
                result.append(text.charAt(i));
            }
        }
        return new Spell(result.toString(), codex);

    }

}
