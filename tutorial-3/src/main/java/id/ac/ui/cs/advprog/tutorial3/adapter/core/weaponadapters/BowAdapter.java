package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

import java.util.concurrent.locks.AbstractOwnableSynchronizer;

// TODO: complete me :)
public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean mode; // false == spontan, true == aim shot

    public BowAdapter(Bow bow){
        this.bow = bow;
        this.mode = false;
    }

    @Override
    public String normalAttack() {
       return bow.shootArrow(mode);
    }

    @Override
    public String chargedAttack() {
        if (mode == false){
            mode = true;
            return "Entered aim shot";
        }
        else{
            mode = false;
            return "Leaving aim shot mode";
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return bow.getHolderName();
    }
}
