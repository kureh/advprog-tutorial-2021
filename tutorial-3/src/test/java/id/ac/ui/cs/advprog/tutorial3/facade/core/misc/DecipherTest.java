package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DecipherTest {
    private Class<?> DecipherClass;
    private Decipher decipher;

    @BeforeEach
    public void setup() throws Exception {
        DecipherClass = Decipher.class;
        decipher = new Decipher();
    }

    @Test
    public void testCipherHasEncodeMethod() throws Exception {
        Method encode = DecipherClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = encode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCipherEncodeResult() {
        String expected = "(@wp->N^^q";
        assertEquals(expected, decipher.encode("Test 12345"));
    }

    @Test
    public void testCipherHasDecodeMethod() throws Exception {
        Method decode = DecipherClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = decode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCipherDecodeResult() {
        String expected = "Test 12345";
        assertEquals(expected, decipher.decode("(@wp->N^^q"));
    }


}
